import { Component, OnInit } from '@angular/core';

export interface ParseUser {
  id: string;
  email: string;
  first_name: string;
  last_name: string;
  n_document: string;
  phone_number: string;
}

@Component({
  selector: 'app-prueba',
  templateUrl: './prueba.component.html',
  styleUrls: ['./prueba.component.scss']
})

export class PruebaComponent implements OnInit {
  public users!: ParseUser[];

  
  viewEmail(data){
    data.forEach(element => {
      console.log("El correo del usuario es: ",element.email)
    });
  }

  ngOnInit(): void {
    fetch('./assets/users.json').then(res => res.json())
      .then(json => {
        this.users = json;
        this.viewEmail(json)
      });
  }

}
