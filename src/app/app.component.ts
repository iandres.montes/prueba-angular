import { Component } from '@angular/core';
import { PruebaComponent } from './prueba/prueba.component';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'new-test';
}
